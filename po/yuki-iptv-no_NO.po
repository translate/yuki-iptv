msgid ""
msgstr ""
"Project-Id-Version: yuki-iptv\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-02-19 17:53+0000\n"
"PO-Revision-Date: 2024-02-19 18:05\n"
"Last-Translator: \n"
"Language-Team: Norwegian\n"
"Language: no_NO\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: usr/lib/yuki-iptv/yuki-iptv.py:364 usr/lib/yuki-iptv/yuki-iptv.py:6986
msgid "IPTV player with EPG support"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:635
msgid "Loading TV guide cache..."
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:788 usr/lib/yuki-iptv/yuki-iptv.py:1048
#: usr/lib/yuki-iptv/yuki-iptv.py:1075 usr/lib/yuki-iptv/yuki-iptv.py:1076
#: usr/lib/yuki-iptv/yuki-iptv.py:1077 usr/lib/yuki-iptv/yuki-iptv.py:5214
#: usr/lib/yuki-iptv/yuki-iptv.py:5356 usr/lib/yuki-iptv/yuki-iptv.py:5544
#: usr/lib/yuki-iptv/yuki_iptv/m3u.py:35 usr/lib/yuki-iptv/yuki_iptv/xspf.py:30
#: usr/lib/yuki-iptv/yuki_iptv/xtreamtom3u.py:31
msgid "All channels"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:875 usr/lib/yuki-iptv/yuki-iptv.py:891
#: usr/lib/yuki-iptv/yuki_iptv/qt.py:76
msgid "yuki-iptv error"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:878
msgid "Processing error"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:884 usr/lib/yuki-iptv/yuki-iptv.py:896
#: usr/lib/yuki-iptv/yuki_iptv/m3u_editor.py:169
#: usr/lib/yuki-iptv/yuki_iptv/qt.py:83
msgid "Error"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:892
msgid "Could not connect"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:922 usr/lib/yuki-iptv/yuki-iptv.py:976
#: usr/lib/yuki-iptv/yuki-iptv.py:985
msgid "Failed to load playlist - unknown encoding! Please use playlists in UTF-8 encoding."
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:994 usr/lib/yuki-iptv/yuki-iptv.py:1036
#: usr/lib/yuki-iptv/yuki_iptv/m3u_editor.py:122
msgid "Playlist loading error!"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:1077 usr/lib/yuki-iptv/yuki-iptv.py:5357
#: usr/lib/yuki-iptv/yuki-iptv.py:6006
msgid "Favourites"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:1156 usr/lib/yuki-iptv/yuki-iptv.py:1935
#: usr/lib/yuki-iptv/yuki-iptv.py:7873 usr/lib/yuki-iptv/yuki-iptv.py:9154
msgid "Settings"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:1161
#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:372
msgid "Shortcuts"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:1177
msgid "Description"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:1177
msgid "Shortcut"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:1194 usr/lib/yuki-iptv/yuki-iptv.py:3896
#: usr/lib/yuki-iptv/yuki-iptv.py:5968
msgid "Are you sure?"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:1221 usr/lib/yuki-iptv/yuki-iptv.py:3785
msgid "Double click to change"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:1234
msgid "Reset to defaults"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:1242
msgid "Modify shortcut"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:1257
msgid "Press the key combination\n"
"you want to assign"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:1309
msgid "Shortcut already used"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:1314
msgid "Cancel"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:1316
msgid "OK"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:1346
#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:380
msgid "&About yuki-iptv"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:1351 usr/lib/yuki-iptv/yuki-iptv.py:3716
msgid "License"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:1356 usr/lib/yuki-iptv/yuki-iptv.py:6011
msgid "Video settings"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:1361 usr/lib/yuki-iptv/yuki-iptv.py:6010
msgid "Open in external player"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:1366 usr/lib/yuki-iptv/yuki-iptv.py:1568
#: usr/lib/yuki-iptv/yuki-iptv.py:6004 usr/lib/yuki-iptv/yuki-iptv.py:6389
#: usr/lib/yuki-iptv/yuki-iptv.py:6469 usr/lib/yuki-iptv/yuki-iptv.py:7885
#: usr/lib/yuki-iptv/yuki-iptv.py:9156
#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:362
msgid "TV guide"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:1373 usr/lib/yuki-iptv/yuki-iptv.py:1383
#: usr/lib/yuki-iptv/yuki-iptv.py:6635
msgid "No TV guide for channel"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:1405 usr/lib/yuki-iptv/yuki-iptv.py:2277
#: usr/lib/yuki-iptv/yuki-iptv.py:5750 usr/lib/yuki-iptv/yuki-iptv.py:6480
msgid "Search channel"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:1407 usr/lib/yuki-iptv/yuki-iptv.py:1862
#: usr/lib/yuki-iptv/yuki-iptv.py:1864 usr/lib/yuki-iptv/yuki-iptv.py:2279
#: usr/lib/yuki-iptv/yuki-iptv.py:6482
msgid "Search"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:1419
msgid "Show only channels in playlist"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:1516 usr/lib/yuki-iptv/yuki-iptv.py:7865
#: usr/lib/yuki-iptv/yuki-iptv.py:9155
msgid "Archive"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:1553 usr/lib/yuki-iptv/yuki-iptv.py:7843
msgid "Recording scheduler"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:1558 usr/lib/yuki-iptv/yuki-iptv.py:1563
#: usr/lib/yuki-iptv/yuki-iptv.py:7879
msgid "Playlists"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:1582 usr/lib/yuki-iptv/yuki-iptv.py:1615
msgid "Expiration date"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:1592 usr/lib/yuki-iptv/yuki-iptv.py:7362
#: usr/lib/yuki-iptv/yuki-iptv.py:7363
msgid "Unknown"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:1733
msgid "Name should not be empty!"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:1741
msgid "URL not specified!"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:1749
#: usr/lib/yuki-iptv/yuki_iptv/m3u_editor.py:95
msgid "Select m3u playlist"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:1759
msgid "Select EPG file"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:1766
msgid "Name"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:1767 usr/lib/yuki-iptv/yuki-iptv.py:3111
msgid "M3U / XSPF playlist"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:1768 usr/lib/yuki-iptv/yuki-iptv.py:3113
msgid "TV guide\n"
"address"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:1771 usr/lib/yuki-iptv/yuki-iptv.py:1773
#: usr/lib/yuki-iptv/yuki-iptv.py:3139 usr/lib/yuki-iptv/yuki-iptv.py:3142
msgid "Path to file or URL"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:1784 usr/lib/yuki-iptv/yuki-iptv.py:3617
msgid "Save"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:1792
msgid "TV guide offset"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:1794
#, python-format
msgid "%d hour"
msgid_plural "%d hours"
msgstr[0] ""
msgstr[1] ""

#: usr/lib/yuki-iptv/yuki-iptv.py:1859 usr/lib/yuki-iptv/yuki-iptv.py:2460
#: usr/lib/yuki-iptv/yuki-iptv.py:2478 usr/lib/yuki-iptv/yuki-iptv.py:2864
#: usr/lib/yuki-iptv/yuki-iptv.py:5877 usr/lib/yuki-iptv/yuki-iptv.py:5880
#: usr/lib/yuki-iptv/yuki-iptv.py:5895
msgid "Default"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:1908
msgid "Open"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:1929
msgid "Select"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:1931 usr/lib/yuki-iptv/yuki-iptv.py:2257
msgid "Add"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:1932
msgid "Edit"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:1933
msgid "Delete"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:1934
msgid "Favourites+"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:2039
msgid "Channel"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:2040 usr/lib/yuki-iptv/yuki-iptv.py:2295
msgid "Start record time"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:2041 usr/lib/yuki-iptv/yuki-iptv.py:2296
msgid "End record time"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:2134 usr/lib/yuki-iptv/yuki-iptv.py:9153
msgid "Scheduler"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:2171
msgid "No planned recordings"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:2178 usr/lib/yuki-iptv/yuki-iptv.py:8398
msgid "Waiting for record"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:2219
msgid "Recording"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:2220
msgid "Status"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:2247
msgid "Planned recordings"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:2248
msgid "Active recordings"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:2253
msgid "Choose channel"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:2259
msgid "Remove"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:2311
msgid "Post-recording\n"
"action"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:2313
msgid "Nothing to do"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:2314
msgid "Press Stop"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:2368
msgid "Recording of two channels simultaneously is not available!"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:2393
msgid "Select folder for recordings and screenshots"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:2408 usr/lib/yuki-iptv/yuki-iptv.py:3114
msgid "Deinterlace"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:2409 usr/lib/yuki-iptv/yuki-iptv.py:3219
msgid "User agent"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:2410 usr/lib/yuki-iptv/yuki-iptv.py:5541
#: usr/lib/yuki-iptv/yuki-iptv.py:5544
msgid "Group"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:2412
msgid "Hide"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:2431
msgid "Contrast"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:2432
msgid "Brightness"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:2433
msgid "Hue"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:2434
msgid "Saturation"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:2435 usr/lib/yuki-iptv/yuki-iptv.py:2566
msgid "Gamma"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:2436 usr/lib/yuki-iptv/yuki-iptv.py:3374
msgid "Aspect ratio"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:2437 usr/lib/yuki-iptv/yuki-iptv.py:3375
msgid "Scale / Zoom"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:2438 usr/lib/yuki-iptv/yuki-iptv.py:3376
msgid "Pan and scan"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:2439
msgid "EPG name"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:2509 usr/lib/yuki-iptv/yuki-iptv.py:3220
msgid "HTTP Referer:"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:2517 usr/lib/yuki-iptv/yuki-iptv.py:2518
#: usr/lib/yuki-iptv/yuki-iptv.py:2535 usr/lib/yuki-iptv/yuki-iptv.py:2539
#: usr/lib/yuki-iptv/yuki-iptv.py:7077 usr/lib/yuki-iptv/yuki-iptv.py:7083
#: usr/lib/yuki-iptv/yuki-iptv.py:7097 usr/lib/yuki-iptv/yuki-iptv.py:7101
msgid "Average Bitrate"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:2535 usr/lib/yuki-iptv/yuki-iptv.py:2539
#: usr/lib/yuki-iptv/yuki-iptv.py:2553 usr/lib/yuki-iptv/yuki-iptv.py:2558
#: usr/lib/yuki-iptv/yuki-iptv.py:2580 usr/lib/yuki-iptv/yuki-iptv.py:2595
#: usr/lib/yuki-iptv/yuki-iptv.py:2601 usr/lib/yuki-iptv/yuki-iptv.py:2613
#: usr/lib/yuki-iptv/yuki-iptv.py:2652 usr/lib/yuki-iptv/yuki-iptv.py:2656
#: usr/lib/yuki-iptv/yuki-iptv.py:7078 usr/lib/yuki-iptv/yuki-iptv.py:7084
#: usr/lib/yuki-iptv/yuki-iptv.py:7113 usr/lib/yuki-iptv/yuki-iptv.py:7115
#: usr/lib/yuki-iptv/yuki-iptv.py:7127 usr/lib/yuki-iptv/yuki-iptv.py:7133
msgid "General"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:2536 usr/lib/yuki-iptv/yuki-iptv.py:2540
#: usr/lib/yuki-iptv/yuki-iptv.py:8460
msgid "kbps"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:2554
msgid "Dimensions"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:2558
msgid "Aspect"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:2562 usr/lib/yuki-iptv/yuki-iptv.py:2566
#: usr/lib/yuki-iptv/yuki-iptv.py:2570 usr/lib/yuki-iptv/yuki-iptv.py:2653
#: usr/lib/yuki-iptv/yuki-iptv.py:7114 usr/lib/yuki-iptv/yuki-iptv.py:7130
msgid "Color"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:2563
msgid "Pixel Format"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:2571
msgid "Bits Per Pixel"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:2580 usr/lib/yuki-iptv/yuki-iptv.py:2613
msgid "Codec"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:2591
msgid "surround sound"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:2592 usr/lib/yuki-iptv/yuki-iptv.py:2603
#: usr/lib/yuki-iptv/yuki-iptv.py:2657 usr/lib/yuki-iptv/yuki-iptv.py:7116
#: usr/lib/yuki-iptv/yuki-iptv.py:7136
msgid "Layout"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:2592
msgid "Channels"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:2595
msgid "Sample Rate"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:2601
msgid "Format"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:2604
msgid "Channel Count"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:2808 usr/lib/yuki-iptv/yuki-iptv.py:6084
#: usr/lib/yuki-iptv/yuki-iptv.py:8517
msgid "Loading..."
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:2887 usr/lib/yuki-iptv/yuki-iptv.py:3163
msgid "Save settings"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:3112
msgid "Update playlist\n"
"at launch"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:3115
msgid "Hardware\n"
"acceleration"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:3116
msgid "Cache"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:3117
msgid "UDP proxy"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:3119
msgid "Folder for recordings\n"
"and screenshots"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:3159
#, python-format
msgid "%d second"
msgid_plural "%d seconds"
msgstr[0] ""
msgstr[1] ""

#: usr/lib/yuki-iptv/yuki-iptv.py:3161
msgid "Or select provider"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:3166
msgid "Reset channel settings"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:3168
msgid "Clear logo cache"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:3178 usr/lib/yuki-iptv/yuki-iptv.py:3684
#: usr/lib/yuki-iptv/yuki-iptv.py:3730
msgid "Close"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:3199
#, python-format
msgid "%d day"
msgid_plural "%d days"
msgstr[0] ""
msgstr[1] ""

#: usr/lib/yuki-iptv/yuki-iptv.py:3202
msgid "Load EPG for"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:3228
msgid "mpv options"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:3229
msgid "list"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:3238
msgid "Do not update\n"
"EPG at boot"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:3243
msgid "Open previous channel\n"
"at startup"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:3245
msgid "Hide mpv panel"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:3247
msgid "Hide EPG percentage"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:3250
msgid "Hide EPG from playlist"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:3253
msgid "Multicast optimization"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:3256
msgid "Hide bitrate / video info"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:3259
msgid "Enable styles redefinition"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:3261
msgid "Volume change step"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:3281 usr/lib/yuki-iptv/yuki-iptv.py:3284
#: usr/lib/yuki-iptv/yuki-iptv.py:3332 usr/lib/yuki-iptv/yuki-iptv.py:3335
#: usr/lib/yuki-iptv/yuki-iptv.py:3546
msgid "WARNING: experimental function, working with problems"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:3299
msgid "Floating panels opacity"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:3308
msgid "Floating panel\n"
"position"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:3311
msgid "Right"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:3312
msgid "Left"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:3316
msgid "Switch channels with\n"
"the mouse wheel"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:3319
msgid "Automatic\n"
"reconnection"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:3322
msgid "by default:\n"
"change volume"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:3340
msgid "Show playlist\n"
"on mouse move"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:3345
msgid "Show controls\n"
"on mouse move"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:3350
msgid "Channel logos"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:3352
msgid "Prefer M3U"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:3353
msgid "Prefer EPG"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:3354
msgid "Do not load from EPG"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:3355
msgid "Do not load any logos"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:3358
msgid "Do not cache EPG"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:3363
msgid "Do not create screenshots\n"
"and recordings subfolders"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:3369
msgid "Hide the current television program"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:3396
msgid "Enable catchup"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:3400
msgid "Enable rewind"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:3404
msgid "Hide channel logos"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:3420
msgid "Main"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:3421 usr/lib/yuki-iptv/yuki-iptv.py:7120
msgid "Video"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:3422
msgid "Network"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:3423
msgid "GUI"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:3424
msgid "Actions"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:3425
msgid "Catchup"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:3426
msgid "EPG"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:3427
msgid "Other"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:3428
msgid "Debug"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:3625
msgid "Username"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:3627
msgid "Password"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:3629
msgid "URL"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:3726
msgid "About Qt"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:4148 usr/lib/yuki-iptv/yuki-iptv.py:7817
msgid "Pause"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:4155
msgid "Play"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:4515 usr/lib/yuki-iptv/yuki-iptv.py:4875
#: usr/lib/yuki-iptv/yuki-iptv.py:6612 usr/lib/yuki-iptv/yuki-iptv.py:7011
#: usr/lib/yuki-iptv/yuki-iptv.py:7146
msgid "No channel selected"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:4546
msgid "Processing record..."
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:4779 usr/lib/yuki-iptv/yuki-iptv.py:5706
#: usr/lib/yuki-iptv/yuki-iptv.py:5995 usr/lib/yuki-iptv/yuki-iptv.py:6213
#: usr/lib/yuki-iptv/yuki-iptv.py:6243 usr/lib/yuki-iptv/yuki-iptv.py:7306
msgid "Nothing found"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:5024 usr/lib/yuki-iptv/yuki-iptv.py:5106
#: usr/lib/yuki-iptv/yuki-iptv.py:7847 usr/lib/yuki-iptv/yuki-iptv.py:8863
msgid "Volume"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:5035
msgid "To exit fullscreen mode press"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:5132 usr/lib/yuki-iptv/yuki-iptv.py:5140
msgid "Volume off"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:5225
msgid "of"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:5762
msgid "Search movie"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:5774
msgid "Search series"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:5904
msgid "Delete from favourites"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:6005
msgid "Hide TV guide"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:6008
msgid "Favourites+ (separate playlist)"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:6247 usr/lib/yuki-iptv/yuki-iptv.py:6270
msgid "Back"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:6305
msgid "TV channels"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:6305
msgid "Movies"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:6305
msgid "Series"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:6495
msgid "Page"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:6592
msgid "Doing screenshot..."
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:6605
msgid "Screenshot saved!"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:6608
msgid "Screenshot saving error!"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:6729
msgid "Open archive"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:6781 usr/lib/yuki-iptv/yuki-iptv.py:6786
msgid "channels"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:6829
msgid "Preparing record"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:6870
msgid "No channel selected for record"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:6967
msgid "Custom MPV options invalid, ignoring them"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:6988
msgid "Using Qt {} ({})"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:6996
msgid "Using libmpv {}"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:7133
msgid "Audio"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:7149 usr/lib/yuki-iptv/yuki-iptv.py:9148
#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:356
msgid "Stream Information"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:7206
msgid "Playing error"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:7821
msgid "Stop"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:7827
msgid "Fullscreen"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:7833
msgid "Open recordings folder"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:7837 usr/lib/yuki-iptv/yuki-iptv.py:9132
msgid "Record"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:7859 usr/lib/yuki-iptv/yuki-iptv.py:9133
msgid "Screenshot"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:7889
msgid "Previous channel"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:7893
msgid "Next channel"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:7912
msgid "Writing EPG cache"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:8091
msgid "Rewind"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:8317 usr/lib/yuki-iptv/yuki_iptv/epg.py:174
msgid "Updating TV guide..."
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:8326 usr/lib/yuki-iptv/yuki-iptv.py:8548
msgid "TV guide update error!"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:8459
msgid "bps"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:8461
msgid "Mbps"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:8462
msgid "Gbps"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:8463
msgid "Tbps"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:8535
msgid "TV guide update done!"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:8546
msgid "EPG is outdated!"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:9114
#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:226
#, python-format
msgid "-%d second"
msgid_plural "-%d seconds"
msgstr[0] ""
msgstr[1] ""

#: usr/lib/yuki-iptv/yuki-iptv.py:9115
#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:227
#, python-format
msgid "+%d second"
msgid_plural "+%d seconds"
msgstr[0] ""
msgstr[1] ""

#: usr/lib/yuki-iptv/yuki-iptv.py:9116 usr/lib/yuki-iptv/yuki-iptv.py:9118
#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:228
#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:230
#, python-format
msgid "-%d minute"
msgid_plural "-%d minutes"
msgstr[0] ""
msgstr[1] ""

#: usr/lib/yuki-iptv/yuki-iptv.py:9117 usr/lib/yuki-iptv/yuki-iptv.py:9119
#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:229
#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:231
#, python-format
msgid "+%d minute"
msgid_plural "+%d minutes"
msgstr[0] ""
msgstr[1] ""

#: usr/lib/yuki-iptv/yuki-iptv.py:9128 usr/lib/yuki-iptv/yuki-iptv.py:9145
#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:313
msgid "V&olume -"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:9129 usr/lib/yuki-iptv/yuki-iptv.py:9146
#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:317
msgid "Vo&lume +"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:9130
#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:244
msgid "&Normal speed"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:9131 usr/lib/yuki-iptv/yuki-iptv.py:9136
msgid "Quit the program"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:9134
msgid "Exit fullscreen"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:9135
#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:366
msgid "&Update TV guide"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:9137
#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:339
msgid "Show/hide playlist"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:9138
#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:345
msgid "Show/hide controls panel"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:9139
#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:272
msgid "&Video settings"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:9140 usr/lib/yuki-iptv/yuki-iptv.py:9141
#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:264
msgid "&Fullscreen"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:9142
#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:309
msgid "&Mute audio"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:9143
#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:200
msgid "&Play / Pause"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:9144
#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:204
msgid "&Stop"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:9147
#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:259
msgid "&Next"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:9149
#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:255
msgid "&Previous"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:9150
#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:190
msgid "&m3u Editor"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:9151
#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:182
msgid "&Playlists"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:9152
#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:186
msgid "&Update current playlist"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:9157
#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:268
msgid "&Compact mode"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:9158
msgid "TV guide for all channels"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:9159
#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:349
msgid "Window always on top"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:9160
#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:208
msgid "&Frame step"
msgstr ""

#: usr/lib/yuki-iptv/yuki-iptv.py:9161
#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:212
msgid "Fra&me back step"
msgstr ""

#: usr/lib/yuki-iptv/yuki_iptv/epg.py:93
msgid "Updating TV guide... (loading {}/{})"
msgstr ""

#: usr/lib/yuki-iptv/yuki_iptv/epg.py:99
#: usr/lib/yuki-iptv/yuki_iptv/epg_xmltv.py:83
msgid "Updating TV guide... (parsing {}/{})"
msgstr ""

#: usr/lib/yuki-iptv/yuki_iptv/epg_xmltv.py:73
msgid "Updating TV guide... (unpacking {}/{})"
msgstr ""

#: usr/lib/yuki-iptv/yuki_iptv/m3u_editor.py:117
msgid "loaded"
msgstr ""

#: usr/lib/yuki-iptv/yuki_iptv/m3u_editor.py:159
msgid "Save File"
msgstr ""

#: usr/lib/yuki-iptv/yuki_iptv/m3u_editor.py:159
msgid "Playlists (*.m3u)"
msgstr ""

#: usr/lib/yuki-iptv/yuki_iptv/m3u_editor.py:167
msgid "Playlist successfully saved!"
msgstr ""

#: usr/lib/yuki-iptv/yuki_iptv/m3u_editor.py:176
msgid "Load M3U"
msgstr ""

#: usr/lib/yuki-iptv/yuki_iptv/m3u_editor.py:180
msgid "Save as"
msgstr ""

#: usr/lib/yuki-iptv/yuki_iptv/m3u_editor.py:185
msgid "File"
msgstr ""

#: usr/lib/yuki-iptv/yuki_iptv/m3u_editor.py:274
msgid "find"
msgstr ""

#: usr/lib/yuki-iptv/yuki_iptv/m3u_editor.py:279
msgid "replace with"
msgstr ""

#: usr/lib/yuki-iptv/yuki_iptv/m3u_editor.py:284
msgid "replace all"
msgstr ""

#: usr/lib/yuki-iptv/yuki_iptv/m3u_editor.py:292
msgid "delete row"
msgstr ""

#: usr/lib/yuki-iptv/yuki_iptv/m3u_editor.py:300
msgid "add row"
msgstr ""

#: usr/lib/yuki-iptv/yuki_iptv/m3u_editor.py:324
msgid "filter group (press Enter)"
msgstr ""

#: usr/lib/yuki-iptv/yuki_iptv/m3u_editor.py:328
msgid "insert search term and press enter\n"
" use Selector → to choose column to search"
msgstr ""

#: usr/lib/yuki-iptv/yuki_iptv/m3u_editor.py:382
msgid "m3u Editor"
msgstr ""

#: usr/lib/yuki-iptv/yuki_iptv/m3u_editor.py:392
#: usr/lib/yuki-iptv/yuki_iptv/m3u_editor.py:418
msgid "Ready"
msgstr ""

#: usr/lib/yuki-iptv/yuki_iptv/m3u_editor.py:398
msgid "Save Confirmation"
msgstr ""

#: usr/lib/yuki-iptv/yuki_iptv/m3u_editor.py:400
msgid "The document was changed.<br>Do you want to save the changes?"
msgstr ""

#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:107
msgid "Error applying filters"
msgstr ""

#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:194
msgid "&Exit"
msgstr ""

#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:276
msgid "&Screenshot"
msgstr ""

#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:281
msgid "&Postprocessing"
msgstr ""

#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:284
msgid "&Deblock"
msgstr ""

#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:287
msgid "De&ring"
msgstr ""

#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:291
msgid "Debanding (&gradfun)"
msgstr ""

#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:295
msgid "Add n&oise"
msgstr ""

#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:298
msgid "Add &black borders"
msgstr ""

#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:301
msgid "Soft&ware scaling"
msgstr ""

#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:304
msgid "&Autodetect phase"
msgstr ""

#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:323
msgid "&Extrastereo"
msgstr ""

#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:326
msgid "&Karaoke"
msgstr ""

#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:330
msgid "&Headphone optimization"
msgstr ""

#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:334
msgid "Volume &normalization"
msgstr ""

#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:375
msgid "&Settings"
msgstr ""

#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:384
#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:386
#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:388
#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:532
msgid "empty"
msgstr ""

#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:428
msgid "&File"
msgstr ""

#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:439
msgid "&Play"
msgstr ""

#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:449
msgid "Speed"
msgstr ""

#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:459
msgid "&Video"
msgstr ""

#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:460
#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:480
#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:495
msgid "&Track"
msgstr ""

#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:465
#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:482
msgid "F&ilters"
msgstr ""

#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:479
msgid "&Audio"
msgstr ""

#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:494
msgid "&Subtitles"
msgstr ""

#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:500
msgid "Vie&w"
msgstr ""

#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:509
msgid "&Options"
msgstr ""

#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:515
msgid "&Help"
msgstr ""

#: usr/lib/yuki-iptv/yuki_iptv/menubar.py:597
msgid "None"
msgstr ""

#: usr/lib/yuki-iptv/yuki_iptv/record.py:98
#: usr/lib/yuki-iptv/yuki_iptv/record.py:138
#: usr/lib/yuki-iptv/yuki_iptv/record.py:146
msgid "ffmpeg crashed!"
msgstr ""

#: usr/lib/yuki-iptv/yuki_iptv/record.py:99
#: usr/lib/yuki-iptv/yuki_iptv/record.py:139
msgid "exit code:"
msgstr ""

