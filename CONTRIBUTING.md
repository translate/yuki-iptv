# Contributions Guidelines

## Code formatting

We're using **[flake8](https://pypi.org/project/flake8/)** and **[black](https://pypi.org/project/black/)**.  
Run ```make lint``` before creating a pull request.  
Run ```make black``` to auto-format code.
